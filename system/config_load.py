import os
from configparser import ConfigParser, ExtendedInterpolation
from system.crypto_functions import random_string

class EnvInterpolation(ExtendedInterpolation):
    """Interpolation which expands environment variables in values."""
    def before_get(self, parser, section, option, value, defaults):
        return os.path.expandvars(value)

config_path = "./configuration/settings.ini"

# Global configuration instance
global_config = ConfigParser(interpolation=EnvInterpolation())
global_config.read(config_path)


def config_dict():
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    return config


def change_secret_key():
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    config.set('main', 'secret', random_string(20))
    with open(config_path, 'w') as configfile:
        config.write(configfile)


def change_basic_password():
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    config.set('security', 'basic_password', random_string(20))
    with open(config_path, 'w') as configfile:
        config.write(configfile)


def change_option(group: str, option_name: str, option_value: str):
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    config.set(group, option_name, option_value)
    with open(config_path, 'w') as configfile:
        config.write(configfile)


def change_db_type(database: str):
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    config.set('database', 'type', str(database))
    with open(config_path, 'w') as configfile:
        config.write(configfile)


def change_external_option(status: bool):
    config = ConfigParser(interpolation=EnvInterpolation())
    config.read(config_path)
    config.set('speedup', 'external_js', str(int(status)))
    config.set('speedup', 'external_css', str(int(status)))
    config.set('speedup', 'external_img', str(int(status)))
    with open(config_path, 'w') as configfile:
        config.write(configfile)


def recover_config():
    pass
    # TODO
